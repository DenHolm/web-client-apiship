package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import project.dto.ordersDto.orderDto.FailedOrderDto;

import java.util.List;

@Data
public class GetLabelDto {

    @JsonProperty(value = "url")
    private String url;

    @JsonProperty(value = "failedOrders")
    private List<FailedOrderDto> failedOrders;
}
