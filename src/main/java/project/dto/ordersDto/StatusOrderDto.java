package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import project.dto.ordersDto.orderDto.OrderInfoDto;
import project.dto.ordersDto.orderDto.StatusDto;

@Data
public class StatusOrderDto {

    @JsonProperty(value = "orderInfo")
    private OrderInfoDto orderInfoDto;

    @JsonProperty(value = "status")
    private StatusDto statusDto;

}
