package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class WaybillItemDto {

    @JsonProperty(value = "providerKey")
    private String providerKey;

    @JsonProperty(value = "file")
    private String file;

    @JsonProperty(value = "orderIds")
    private List<Long> orderIds;
}
