package project.dto.ordersDto.orderDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    @JsonProperty(value ="providerNumber")
    private String providerNumber;

    @JsonProperty(value ="clientNumber")
    private String clientNumber;

    @JsonProperty(value ="barcode")
    private String barcode;

    @JsonProperty(value ="description")
    private String description;

    @JsonProperty(value ="height")
    private Integer height;

    @JsonProperty(value ="length")
    private Integer length;

    @JsonProperty(value ="width")
    private Integer width;

    @JsonProperty(value ="weight")
    private Integer weight;

    @JsonProperty(value ="providerKey")
    private String providerKey;

    @JsonProperty(value ="providerConnectId")
    private Integer providerConnectId;

    @JsonProperty(value ="pickupType")
    private Integer pickupType;

    @JsonProperty(value ="deliveryType")
    private Integer deliveryType;

    @JsonProperty(value ="tariffId")
    private Integer tariffId;

    @JsonProperty(value ="pickupDate")
    private String pickupDate;

    @JsonProperty(value ="deliveryDate")
    private String deliveryDate;

    @JsonProperty(value ="pointInId")
    private Integer pointInId;

    @JsonProperty(value ="pointOutId")
    private Integer pointOutId;

    @JsonProperty(value ="pickupTimeStart")
    private String pickupTimeStart;

    @JsonProperty(value ="pickupTimeEnd")
    private String pickupTimeEnd;

    @JsonProperty(value ="deliveryTimeStart")
    private String deliveryTimeStart;

    @JsonProperty(value ="deliveryTimeEnd")
    private String deliveryTimeEnd;
}
