package project.dto.ordersDto.orderDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlaceDto
{
    @JsonProperty(value ="placeNumber")
    private String placeNumber;

    @JsonProperty(value ="barcode")
    private String barcode;

    @JsonProperty(value ="height")
    private Integer height;

    @JsonProperty(value ="length")
    private Integer length;

    @JsonProperty(value ="width")
    private Integer width;

    @JsonProperty(value ="weight")
    private Integer weight;

    @JsonProperty(value ="items")
    List<ItemDto> itemDtos;
}
