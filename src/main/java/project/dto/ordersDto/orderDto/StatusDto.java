package project.dto.ordersDto.orderDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class StatusDto {

    @JsonProperty(value = "key")
    private String key;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "created")
    private String created;

    @JsonProperty(value = "providerCode")
    private String providerCode;

    @JsonProperty(value = "providerName")
    private String providerName;

    @JsonProperty(value = "providerDescription")
    private String providerDescription;

    @JsonProperty(value = "createdProvider")
    private String createdProvider;

    @JsonProperty(value = "errorCode")
    private String errorCode;
}
