package project.dto.ordersDto.orderDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FailedOrderDto
{
    @JsonProperty(value = "orderId")
    private Long orderId;

    @JsonProperty(value = "message")
    private String message;
}
