package project.dto.ordersDto.orderDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressDto {

    @JsonProperty(value ="addressString")
    private String addressString;

    @JsonProperty(value ="lat")
    private Float lat;

    @JsonProperty(value ="lng")
    private Float lng;

    @JsonProperty(value ="postIndex")
    private String postIndex;

    @JsonProperty(value ="countryCode")
    private String countryCode;

    @JsonProperty(value ="region")
    private String region;

    @JsonProperty(value ="area")
    private String area;

    @JsonProperty(value ="city")
    private String city;

    @JsonProperty(value ="cityGuid")
    private String cityGuid;

    @JsonProperty(value ="street")
    private String street;

    @JsonProperty(value ="house")
    private String house;

    @JsonProperty(value ="block")
    private String block;

    @JsonProperty(value ="office")
    private String office;

    @JsonProperty(value ="companyName")
    private String companyName;

    @JsonProperty(value ="companyInn")
    private String companyInn;

    @JsonProperty(value ="contactName")
    private String contactName;

    @JsonProperty(value ="phone")
    private String phone;

    @JsonProperty(value ="email")
    private String email;

    @JsonProperty(value ="comment")
    private String comment;
}

