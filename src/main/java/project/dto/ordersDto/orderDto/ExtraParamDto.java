package project.dto.ordersDto.orderDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExtraParamDto {

    @JsonProperty(value ="key")
    private String key;

    @JsonProperty(value ="value")
    private String value;
}
