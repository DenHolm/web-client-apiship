package project.dto.ordersDto.orderDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CostDto {

     @JsonProperty(value ="assessedCost")
     private Integer assessedCost;

     @JsonProperty(value ="deliveryCost")
     private Integer deliveryCost;

     @JsonProperty(value ="deliveryCostVat")
     private Integer deliveryCostVat;

     @JsonProperty(value ="codCost")
     private Integer codCost;

     @JsonProperty(value ="isDeliveryPayedByRecipient")
     private boolean isDeliveryPayedByRecipient;
}
