package project.dto.ordersDto.orderDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data

public class OrderInfoDto {

    @JsonProperty(value = "orderId")
    private String orderId;

    @JsonProperty(value = "returnProviderNumber")
    private String returnProviderNumber;

    @JsonProperty(value ="providerNumber")
    private String providerNumber;

    @JsonProperty(value ="clientNumber")
    private String clientNumber;

    @JsonProperty(value ="barcode")
    private String barcode;
}
