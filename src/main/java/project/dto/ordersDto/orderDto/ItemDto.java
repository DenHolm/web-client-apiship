package project.dto.ordersDto.orderDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemDto {

    @JsonProperty(value ="articul")
    private String articul;

    @JsonProperty(value ="markCode")
    private String markCode;

    @JsonProperty(value ="description")
    private String description;

    @JsonProperty(value ="quantity")
    private Integer quantity;

    @JsonProperty(value ="quantityDelivered")
    private Integer quantityDelivered;

    @JsonProperty(value ="height")
    private Integer height;

    @JsonProperty(value ="length")
    private Integer length;

    @JsonProperty(value ="width")
    private Integer width;

    @JsonProperty(value ="weight")
    private Integer weight;

    @JsonProperty(value ="assessedCost")
    private Integer assessedCost;

    @JsonProperty(value ="cost")
    private Integer cost;

    @JsonProperty(value ="costVat")
    private Integer costVat;

    @JsonProperty(value ="barcode")
    private String barcode;
}
