package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderCreatedDto
{
     @JsonProperty(value ="orderId")
     private Integer orderId;

     @JsonProperty(value ="created")
     private LocalDateTime created;
}
