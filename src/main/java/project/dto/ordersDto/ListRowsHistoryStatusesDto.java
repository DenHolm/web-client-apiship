package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ListRowsHistoryStatusesDto {

    @JsonProperty(value = "rows")
    private List<ListStatusOrderDto> rows;

    @JsonProperty(value = "meta")
    private MetaDto metaDto;
}
