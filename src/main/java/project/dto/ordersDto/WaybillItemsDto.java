package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import project.dto.ordersDto.orderDto.FailedOrderDto;

import java.util.List;

@Data
public class WaybillItemsDto
{
    @JsonProperty(value = "waybillItems")
    private List<WaybillItemDto> waybillItemDtos;

    @JsonProperty(value = "failedOrders")
    private List<FailedOrderDto> failedOrderDtos;

}
