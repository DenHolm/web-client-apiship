package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import project.dto.ordersDto.orderDto.OrderInfoDto;
import project.dto.ordersDto.orderDto.StatusDto;

import java.util.List;

@Data
public class OrderHistoryStatusesDto
{
    @JsonProperty(value = "orderInfo")
    private OrderInfoDto orderInfoDto;

    @JsonProperty(value = "rows")
    private List<StatusDto> rows;

    @JsonProperty(value = "meta")
    private MetaDto metaDto;
}
