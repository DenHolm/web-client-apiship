package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import project.dto.ordersDto.orderDto.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostOrderDto
{
    @JsonProperty(value ="order")
    private OrderDto orderDto;

    @JsonProperty(value ="cost")
    private CostDto costDto;

    @JsonProperty(value ="sender")
    private AddressDto sender;

    @JsonProperty(value ="recipient")
    private AddressDto recipient;

    @JsonProperty(value ="returnAddress")
    private AddressDto returnAddressDto;

    @JsonProperty(value ="items")
    private ListItemsDto items;

    @JsonProperty(value ="places")
    private List<PlaceDto> placeDtos;

    @JsonProperty(value ="extraParams")
    private List<ExtraParamDto> extraParamDtos;

}
