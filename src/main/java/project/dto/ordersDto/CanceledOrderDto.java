package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CanceledOrderDto {

    @JsonProperty("orderId")
    private Long orderId;

    @JsonProperty("canceled")
    private String canceled;
}
