package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import project.dto.ordersDto.orderDto.FailedOrderDto;

import java.util.List;

@Data
public class ListSucceedOrdersDto {

    @JsonProperty(value = "succeedOrders")
    List<StatusOrderDto> succeedOrders;

    @JsonProperty(value = "failedOrders")
    List<FailedOrderDto> failedOrderDtos;
}
