package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ListOrderIdsAndFormatDto {

    @JsonProperty(value = "orderIds")
    private List<Long> orderIds;

    @JsonProperty(value = "format")
    private String format;

}
