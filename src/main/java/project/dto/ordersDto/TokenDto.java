package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TokenDto {

    @JsonProperty(value ="accessToken")
    private String accessToken;

    @JsonProperty(value ="expired")
    private String expires;
}
