package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import project.dto.ordersDto.orderDto.ItemDto;

import java.util.List;

@Data
public class ListItemsDto {

    @JsonProperty(value ="items")
    private List<ItemDto> items;
}
