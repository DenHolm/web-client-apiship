package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeletedOrderDto {

    @JsonProperty("orderId")
    private Long orderId;

    @JsonProperty("deleted")
    private String deleted;
}
