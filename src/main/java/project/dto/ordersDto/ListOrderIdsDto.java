package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ListOrderIdsDto {

    @JsonProperty(value = "orderIds")
    List<Long> orderIds;

}
