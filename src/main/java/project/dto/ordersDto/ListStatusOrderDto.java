package project.dto.ordersDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import project.dto.ordersDto.orderDto.OrderInfoDto;
import project.dto.ordersDto.orderDto.StatusDto;

import java.util.List;

@Data
public class ListStatusOrderDto {

    @JsonProperty(value = "orderInfo")
    private OrderInfoDto orderInfoDto;

    @JsonProperty(value = "statuses")
    private List<StatusDto> statusDtos;
}
