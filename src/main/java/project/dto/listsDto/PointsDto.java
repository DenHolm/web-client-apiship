package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class PointsDto {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "providerKey")
    private String providerKey;

    @JsonProperty(value = "code")
    private String code;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "postIndex")
    private String postIndex;

    @JsonProperty(value = "lat")
    private Double lat;

    @JsonProperty(value = "lng")
    private Double lng;

    @JsonProperty(value = "countryCode")
    private String countryCode;

    @JsonProperty(value = "region")
    private String region;

    @JsonProperty(value = "regionType")
    private String regionType;

    @JsonProperty(value = "city")
    private String city;

    @JsonProperty(value = "cityGuid")
    private String cityGuid;

    @JsonProperty(value = "cityType")
    private String cityType;

    @JsonProperty(value = "area")
    private String area;

    @JsonProperty(value = "street")
    private String street;

    @JsonProperty(value = "streetType")
    private String streetType;

    @JsonProperty(value = "house")
    private String house;

    @JsonProperty(value = "block")
    private String block;

    @JsonProperty(value = "office")
    private String office;

    @JsonProperty(value = "address")
    private String address;

    @JsonProperty(value = "url")
    private String url;

    @JsonProperty(value = "email")
    private String email;

    @JsonProperty(value = "phone")
    private String phone;

    @JsonProperty(value = "timetable")
    private String timetable;

    @JsonProperty(value = "workTime")
    private WorkTimeDto workTime;

    @JsonProperty(value = "fittingRoom")
    private Integer fittingRoom;

    @JsonProperty(value = "cod")
    private Integer cod;

    @JsonProperty(value = "paymentCard")
    private Integer paymentCard;

    @JsonProperty(value = "availableOperation")
    private Integer availableOperation;

    @JsonProperty(value = "type")
    private Integer type;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "photos")
    private List<String> photos;

    @JsonProperty(value = "metro")
    private List<MetroDto> metro;

    @JsonProperty(value = "extra")
    private List<ExtraDto> extra;

    @JsonProperty(value = "limits")
    private LimitsDto limits;

}
