package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BoxberryDto {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "city")
    private String city;

    @JsonProperty(value = "region")
    private String region;

    @JsonProperty(value = "area")
    private String area;

    @JsonProperty(value = "cityGuid")
    private String cityGuid;

    @JsonProperty(value = "zip")
    private String zip;

}
