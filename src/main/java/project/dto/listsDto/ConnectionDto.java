package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.HashMap;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConnectionDto {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "companyId")
    private String companyId;

    @JsonProperty(value = "providerKey")
    private String providerKey;

    @JsonProperty(value = "connectParams")
    private String connectParams;

    @JsonProperty(value = "insuranceRate")
    private Integer insuranceRate;

    @JsonProperty(value = "cashServiceRate")
    private Integer cashServiceRate;

    @JsonProperty(value = "created")
    private String created;

    @JsonProperty(value = "created")
    private String updated;

    @JsonProperty(value = "company")
    private CompanyDto company;


}
