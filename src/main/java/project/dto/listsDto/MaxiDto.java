package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MaxiDto {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "cityCode")
    private Integer cityCode;

    @JsonProperty(value = "cityName")
    private String cityName;

    @JsonProperty(value = "cityGuid")
    private String cityGuid;

    @JsonProperty(value = "regionName")
    private String regionName;

    @JsonProperty(value = "kladrCode")
    private String kladrCode;

    @JsonProperty(value = "cityComment")
    private String cityComment;

    @JsonProperty(value = "flagPvz")
    private Integer zone;

    @JsonProperty(value = "deliveryDays")
    private Short deliveryDays;


}
