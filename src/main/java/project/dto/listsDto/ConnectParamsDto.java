package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ConnectParamsDto {

    @JsonProperty(value = "client")
    private String client;

    @JsonProperty(value = "key")
    private String key;

    @JsonProperty(value = "useCustomInterval")
    private Boolean useCustomInterval;

}
