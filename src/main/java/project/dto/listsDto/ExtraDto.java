package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ExtraDto {

    @JsonProperty(value = "key")
    private String key;

    @JsonProperty(value = "value")
    private String value;
}
