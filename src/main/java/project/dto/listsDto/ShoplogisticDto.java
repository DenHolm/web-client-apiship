package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ShoplogisticDto {

    @JsonProperty(value = "id")
    private Long Id;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "codeId")
    private Integer codeId;

    @JsonProperty(value = "isCourier")
    private Integer isCourier;

    @JsonProperty(value = "isFilial")
    private Integer isFilial;

    @JsonProperty(value = "oblatCode")
    private String oblatCode;

    @JsonProperty(value = "districtCode")
    private String districtCode;

    @JsonProperty(value = "kladrCode")
    private String kladrCode;

    @JsonProperty(value = "regionName")
    private String regionName;

    @JsonProperty(value = "cityName")
    private String cityName;

    @JsonProperty(value = "cityGuid")
    private Integer cityGuid;

}
