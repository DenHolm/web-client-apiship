package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DpdDto {

    @JsonProperty(value = "dpdCityId")
    private Long dpdCityId;

    @JsonProperty(value = "cityGuid")
    private String cityGuid;

    @JsonProperty(value = "cityName")
    private String cityName;

    @JsonProperty(value = "countryCode")
    private String countryCode;

    @JsonProperty(value = "countryName")
    private String countryName;

    @JsonProperty(value = "regionCode")
    private Integer regionCode;

    @JsonProperty(value = "regionName")
    private String  regionName;

    @JsonProperty(value = "abbreviation")
    private String abbreviation;

    @JsonProperty(value = "isCodCost")
    private Integer isCodCost;

}
