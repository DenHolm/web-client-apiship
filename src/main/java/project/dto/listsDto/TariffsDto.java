package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TariffsDto {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "providerKey")
    private String key;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "aliasName")
    private String aliasName;

    @JsonProperty(value = "weightMin")
    private Integer weightMin;

    @JsonProperty(value = "weightMax")
    private Integer weightMax;

    @JsonProperty(value = "pickupType")
    private Integer pickupType;

    @JsonProperty(value = "deliveryType")
    private Integer deliveryType;

}
