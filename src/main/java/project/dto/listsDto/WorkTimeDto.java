package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class WorkTimeDto {

    @JsonProperty(value = "1")
    private String one;

    @JsonProperty(value = "2")
    private String two;

    @JsonProperty(value = "3")
    private String three;

    @JsonProperty(value = "4")
    private String four;

    @JsonProperty(value = "5")
    private String five;

    @JsonProperty(value = "6")
    private String six;

    @JsonProperty(value = "7")
    private String seven;

}
