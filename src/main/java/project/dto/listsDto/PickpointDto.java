package project.dto.listsDto;


import com.fasterxml.jackson.annotation.JsonProperty;

public class PickpointDto {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "ownerId")
    private Integer ownerId;

    @JsonProperty(value = "code")
    private Integer code;

    @JsonProperty(value = "cityName")
    private String cityName;

    @JsonProperty(value = "cityGuid")
    private String cityGuid;

    @JsonProperty(value = "regionName")
    private String regionName;

}

