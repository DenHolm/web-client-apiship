package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CdekDto {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "cdekId")
    private Integer cdekId;

    @JsonProperty(value = "fullName")
    private String fullName;

    @JsonProperty(value = "cityName")
    private String cityName;

    @JsonProperty(value = "oblName")
    private String oblName;

    @JsonProperty(value = "countryCode")
    private String countryCode;

    @JsonProperty(value = "cityGuid")
    private String cityGuid;

    @JsonProperty(value = "postCodeList")
    private String postCodeList;

    @JsonProperty(value = "codCostLimit")
    private String codCostLimit;

    @JsonProperty(value = "isCentre")
    private Integer isCentre;



}
