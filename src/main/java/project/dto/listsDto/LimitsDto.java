package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class LimitsDto {

    @JsonProperty(value = "maxSizeA")
    private Integer maxSizeA;

    @JsonProperty(value = "maxSizeB")
    private Integer maxSizeB;

    @JsonProperty(value = "maxSizeC")
    private Integer maxSizeC;

    @JsonProperty(value = "maxSizeSum")
    private Integer maxSizeSum;

    @JsonProperty(value = "minWeight")
    private Integer minWeight;

    @JsonProperty(value = "maxWeight")
    private Integer maxWeight;

    @JsonProperty(value = "maxCod")
    private Float maxCod;

    @JsonProperty(value = "maxVolume")
    private Long maxVolume;
}
