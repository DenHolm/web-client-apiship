package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CompanyDto {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "created")
    private Long created;

    @JsonProperty(value = "created")
    private Long updated;

    @JsonProperty(value = "phone")
    private String phone;

    @JsonProperty(value = "deleted")
    private Integer deleted;

    @JsonProperty(value = "deletedAt")
    private String deletedAt;
}
