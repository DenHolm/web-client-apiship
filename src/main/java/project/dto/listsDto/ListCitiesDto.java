package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import project.dto.ordersDto.MetaDto;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListCitiesDto
{
    @JsonProperty(value = "rows")
    private List<?> rows;

    @JsonProperty(value = "meta")
    private MetaDto meta;
}
