package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SpsrDto {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "cityId")
    private Integer cityId;

    @JsonProperty(value = "cityOwnerId")
    private Integer cityOwnerId;

    @JsonProperty(value = "cityName")
    private String cityName;

    @JsonProperty(value = "regionName")
    private String regionName;

    @JsonProperty(value = "countryName")
    private String countryName;

    @JsonProperty(value = "cityGuid")
    private String cityGuid;
}
