package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ServicesDto {

    @JsonProperty(value = "providerKey")
    private String key;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "extraParamName")
    private String extraParamName;

    @JsonProperty(value = "valueDescription")
    private String valueDescription;

    @JsonProperty(value = "description")
    private String description;

}
