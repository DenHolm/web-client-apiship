package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MetroDto {

    @JsonProperty(value = "distance")
    private Float distance;

    @JsonProperty(value = "line")
    private String line;

    @JsonProperty(value = "name")
    private String name;
}
