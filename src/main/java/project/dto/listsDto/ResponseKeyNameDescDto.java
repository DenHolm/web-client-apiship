package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ResponseKeyNameDescDto {

    @JsonProperty(value = "key")
    private String key;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "description")
    private String description;

}
