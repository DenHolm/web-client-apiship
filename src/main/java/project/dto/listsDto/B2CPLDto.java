package project.dto.listsDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class B2CPLDto {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "region")
    private String region;

    @JsonProperty(value = "residence")
    private String residence;

    @JsonProperty(value = "zipFirst")
    private String zipFirst;

    @JsonProperty(value = "zipLast")
    private String zipLast;

    @JsonProperty(value = "transportDays")
    private Integer transportDays;

    @JsonProperty(value = "flagCourier")
    private String flagCourier;

    @JsonProperty(value = "flagPvz")
    private String flagPvz;

    @JsonProperty(value = "flagAvia")
    private String flagAvia;

    @JsonProperty(value = "cityGuid")
    private String cityGuid;

}
