package project.dto.calculateDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CostDto {

    @JsonProperty(value = "providerKey")
    private String providerKey;

    @JsonProperty(value = "tariffs")
    private List<TariffDto> tariffDtos;
}
