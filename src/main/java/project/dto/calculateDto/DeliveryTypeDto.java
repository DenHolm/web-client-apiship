package project.dto.calculateDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class DeliveryTypeDto {

    @JsonProperty(value = "deliveryToDoor")
    private List<CostDto> deliveryToDoor;

    @JsonProperty(value = "deliveryToPoint")
    private List<CostDto> deliveryToPoint;

}
