package project.dto.calculateDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class TariffDto {

    @JsonProperty(value = "tariffProviderId")
    private String tariffProviderId;

    @JsonProperty(value = "tariffId")
    private Integer tariffId;

    @JsonProperty(value = "tariffName")
    private String tariffName;

    @JsonProperty(value = "pickupTypes")
    private List<Integer> pickupTypes;

    @JsonProperty(value = "deliveryTypes")
    private List<Integer> deliveryTypes;

    @JsonProperty(value = "deliveryCost")
    private Integer deliveryCost;

    @JsonProperty(value = "feesIncluded")
    private Boolean feesIncluded;

    @JsonProperty(value = "insuranceFee")
    private Integer insuranceFee;

    @JsonProperty(value = "cashServiceFee")
    private Integer cashServiceFee;

    @JsonProperty(value = "maxDays")
    private Integer maxDays;

    @JsonProperty(value = "minDays")
    private Integer minDays;

}
