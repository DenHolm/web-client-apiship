package project.dto.calculateDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AddressDto {

    @JsonProperty(value ="cityGuid")
    private String cityGuid;

    @JsonProperty(value ="region")
    private String region;

    @JsonProperty(value ="city")
    private String city;

    @JsonProperty(value ="countryCode")
    private String countryCode;

    @JsonProperty(value ="addressString")
    private String addressString;

    @JsonProperty(value ="lat")
    private Float lat;

    @JsonProperty(value ="lng")
    private Float lng;

}
