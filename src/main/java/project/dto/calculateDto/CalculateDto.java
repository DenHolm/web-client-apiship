package project.dto.calculateDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class CalculateDto {

    @JsonProperty(value = "from")
    private AddressDto from;

    @JsonProperty(value = "to")
    private AddressDto to;

    @JsonProperty(value = "weight")
    private Integer weight;

    @JsonProperty(value = "width")
    private Integer width;

    @JsonProperty(value = "height")
    private Integer height;

    @JsonProperty(value = "length")
    private Integer length;

    @JsonProperty(value = "assessedCost")
    private Float assessedCost;

    @JsonProperty(value = "pickupDate")
    private String pickupDate;

    @JsonProperty(value = "pickupTypes")
    private List<Integer> pickupTypes;

    @JsonProperty(value = "deliveryTypes")
    private List<Integer> deliveryTypes;

    @JsonProperty(value = "codCost")
    private Long codCost;

    @JsonProperty(value = "includeFees")
    private Boolean includeFees;

    @JsonProperty(value = "providerKeys")
    private List<String> providerKeys;

    @JsonProperty(value = "timeout")
    private Integer timeout;

    @JsonProperty(value = "extraParams")
    private Map<String, Integer> extraParams;

}
