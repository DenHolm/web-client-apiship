package project.dto.courierCallDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CourierCallDto {

    @JsonProperty(value ="providerKey")
    private String providerKey;

    @JsonProperty(value ="providerConnectId")
    private Integer providerConnectId;

    @JsonProperty(value = "date")
    private String date;

    @JsonProperty(value = "timeStart")
    private String timeStart;

    @JsonProperty(value = "timeEnd")
    private String timeEnd;

    @JsonProperty(value ="height")
    private Integer height;

    @JsonProperty(value ="length")
    private Integer length;

    @JsonProperty(value ="width")
    private Integer width;

    @JsonProperty(value ="weight")
    private Integer weight;

    @JsonProperty(value = "orderIds")
    List<Integer> orderIds;

    @JsonProperty(value ="postIndex")
    private String postIndex;

    @JsonProperty(value ="countryCode")
    private String countryCode;

    @JsonProperty(value ="region")
    private String region;

    @JsonProperty(value ="area")
    private String area;

    @JsonProperty(value ="city")
    private String city;

    @JsonProperty(value ="cityGuid")
    private String cityGuid;

    @JsonProperty(value ="street")
    private String street;

    @JsonProperty(value ="house")
    private String house;

    @JsonProperty(value ="block")
    private String block;

    @JsonProperty(value ="office")
    private String office;

    @JsonProperty(value ="companyName")
    private String companyName;

    @JsonProperty(value ="contactName")
    private String contactName;

    @JsonProperty(value ="phone")
    private String phone;

    @JsonProperty(value ="email")
    private String email;

}
