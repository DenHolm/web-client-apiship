package project.dto.courierCallDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CourierCallCreated {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "providerNumber")
    private String providerNumber;

    @JsonProperty(value = "created")
    private String created;
}
