package project.dto.courierCallDto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CourierCallCanceled {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "canceled")
    private String canceled;
}
