package project.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import project.dto.courierCallDto.CourierCallCanceled;
import project.dto.courierCallDto.CourierCallCreated;
import project.dto.courierCallDto.CourierCallDto;
import project.service.CourierCallService;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
public class ApiCourierCallController {

    @Autowired
    CourierCallService courierCallService;

    @PostMapping(value = "/courierCall")
    public ResponseEntity<Mono<CourierCallCreated>> postCourierCall(@RequestBody CourierCallDto courierCallDto) {

        Mono<CourierCallCreated> answer = courierCallService.postCourierCall(courierCallDto);

        return ResponseEntity.ok(answer);
    }

    @PostMapping(value = "/courierCall/{courierCallId}/cancel")
    public ResponseEntity<Mono<CourierCallCanceled>> postCancelCourier(@PathVariable("courierCallId") Long courierCallId) {

        Mono<CourierCallCanceled> answer = courierCallService.postCancelCourier(courierCallId);

        return ResponseEntity.ok(answer);
    }
}
