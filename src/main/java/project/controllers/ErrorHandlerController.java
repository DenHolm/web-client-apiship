package project.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import project.exceptions.MyException;

@ControllerAdvice
public class ErrorHandlerController {

    @ExceptionHandler({MyException.class})
    public ResponseEntity<?> badRequestException(MyException e) {
        if(e.getError().getCode().matches("0400\\d\\d")){
            return ResponseEntity.status(400).body(e.getError());
        }
        if(e.getError().getCode().matches("0401\\d\\d")){
            return ResponseEntity.status(401).body(e.getError());
        }
        if(e.getError().getCode().matches("0403\\d\\d")){
            return ResponseEntity.status(403).body(e.getError());
        }
        if(e.getError().getCode().matches("0404\\d\\d")){
            return ResponseEntity.status(404).body(e.getError());
        }

        return ResponseEntity.status(415).body("ошибка не обнаружена");
    }
}
