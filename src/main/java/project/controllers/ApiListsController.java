package project.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import project.dto.listsDto.ConnectionDto;
import project.dto.listsDto.ListCitiesDto;
import project.dto.listsDto.TypesDto;
import project.dto.listsDto.ServicesDto;
import project.service.ListsService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
public class ApiListsController {

    @Autowired
    ListsService listsService;

    /** Получение списка статусов */
    @GetMapping("/lists/statuses")
    public ResponseEntity getListStatuses(@RequestParam(value = "limit", required = false) Integer limit,
                                          @RequestParam(value = "offset", required = false) Integer offset,
                                          @RequestParam(value = "filter", required = false) String filter,
                                          @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListStatuses(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }

    /** Получение списка статусов */
    @GetMapping("/lists/providers")
    public ResponseEntity getListProviders(@RequestParam(value = "limit", required = false) Integer limit,
                                          @RequestParam(value = "offset", required = false) Integer offset,
                                          @RequestParam(value = "filter", required = false) String filter,
                                          @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListProviders(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }

    /** Получение списка дополнительных услуг  */
    @GetMapping("/lists/services")
    public ResponseEntity getListServices(@RequestParam(value = "providerKey", required = false) String providerKey)
    {
        Flux<ServicesDto> answer = listsService.getListServices(providerKey);

        return ResponseEntity.ok(answer);
    }

    /** Получение возможных параметров для подключения к службе доставки */
    @GetMapping("/lists/providers/{providerKey}/params")
    public ResponseEntity getListProvidersParams(@PathVariable("providerKey") String providerKey)
    {
        Mono<String> answer = listsService.getListProvidersParams(providerKey);

        return ResponseEntity.ok(answer);
    }

   /** Получение списка пунктов приема/выдачи */
    @GetMapping("/lists/points")
    public ResponseEntity getListPoints(@RequestParam(value = "limit", required = false) Integer limit,
                                        @RequestParam(value = "offset", required = false) Integer offset,
                                        @RequestParam(value = "filter", required = false) String filter,
                                        @RequestParam(value = "fields", required = false) String fields,
                                        @RequestParam(value = "stateCheckOff", required = false) Boolean stateCheckOff)
    {

        Mono<ListCitiesDto> answer = listsService.getListPoints(limit, offset, filter, fields, stateCheckOff);

        return ResponseEntity.ok(answer);
    }

    /** Получение списка актуальных тарифов */
    @GetMapping("/lists/tariffs")
    public ResponseEntity getListTariffs(@RequestParam(value = "limit", required = false) Integer limit,
                                         @RequestParam(value = "offset", required = false) Integer offset,
                                         @RequestParam(value = "filter", required = false) String filter,
                                         @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListTariffs(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }

    /** Получение списка типов приема */
    @GetMapping("/lists/pickupTypes")
    public ResponseEntity getPickupTypes()
    {
        Flux<TypesDto> answer = listsService.getPickupTypes();

        return ResponseEntity.ok(answer);
    }

    /** Получение списка типов доставки */
    @GetMapping("/lists/deliveryTypes")
    public ResponseEntity getDeliveryTypes()
    {
        Flux<TypesDto> answer = listsService.getDeliveryTypes();

        return ResponseEntity.ok(answer);
    }

    /** Получение списка способов оплаты */
    @GetMapping("/lists/paymentMethods")
    public ResponseEntity getPaymentMethods()
    {
        Flux<TypesDto> answer = listsService.getPaymentMethods();

        return ResponseEntity.ok(answer);
    }

    /** Получение списка типов операций для точек приема/выдачи товаров */
    @GetMapping("/lists/operationTypes")
    public ResponseEntity getOperationTypes()
    {
        Flux<TypesDto> answer = listsService.getOperationTypes();

        return ResponseEntity.ok(answer);
    }

    /** Получение списка типов точек приема/выдачи товаров*/
    @GetMapping("/lists/pointTypes")
    public ResponseEntity getPointTypes()
    {
        Flux<TypesDto> answer = listsService.getPointTypes();

        return ResponseEntity.ok(answer);
    }

    /**  Получение списка городов b2cpl */
    @GetMapping("/lists/providerCities/b2cpl")
    public ResponseEntity getListCitiesB2cpl(@RequestParam(value = "limit", required = false) Integer limit,
                                           @RequestParam(value = "offset", required = false) Integer offset,
                                           @RequestParam(value = "filter", required = false) String filter,
                                           @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListCitiesB2cpl(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }

    /**  Получение списка городов boxberry */
    @GetMapping("/lists/providerCities/boxberry")
    public ResponseEntity getListCitiesBoxberry(@RequestParam(value = "limit", required = false) Integer limit,
                                             @RequestParam(value = "offset", required = false) Integer offset,
                                             @RequestParam(value = "filter", required = false) String filter,
                                             @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListCitiesBoxberry(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }

    /**  Получение списка городов dpd */
    @GetMapping("/lists/providerCities/dpd")
    public ResponseEntity getListCitiesDpd(@RequestParam(value = "limit", required = false) Integer limit,
                                                @RequestParam(value = "offset", required = false) Integer offset,
                                                @RequestParam(value = "filter", required = false) String filter,
                                                @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListCitiesDpd(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }

    /**  Получение списка городов iml */
    @GetMapping("/lists/providerCities/iml")
    public ResponseEntity getListCitiesIml(@RequestParam(value = "limit", required = false) Integer limit,
                                           @RequestParam(value = "offset", required = false) Integer offset,
                                           @RequestParam(value = "filter", required = false) String filter,
                                           @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListCitiesIml(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }

    /**  Получение списка городов maxi */
    @GetMapping("/lists/providerCities/maxi")
    public ResponseEntity getListCitiesMaxi(@RequestParam(value = "limit", required = false) Integer limit,
                                           @RequestParam(value = "offset", required = false) Integer offset,
                                           @RequestParam(value = "filter", required = false) String filter,
                                           @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListCitiesMaxi(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }

    /**  Получение списка городов pickpoint */
    @GetMapping("/lists/providerCities/pickpoint")
    public ResponseEntity getListCitiesPickpoint(@RequestParam(value = "limit", required = false) Integer limit,
                                            @RequestParam(value = "offset", required = false) Integer offset,
                                            @RequestParam(value = "filter", required = false) String filter,
                                            @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListCitiesPickpoint(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }


    /**  Получение списка городов Shoplogistic */
    @GetMapping("/lists/providerCities/shoplogistic")
    public ResponseEntity getListCitiesShoplogistic(@RequestParam(value = "limit", required = false) Integer limit,
                                                 @RequestParam(value = "offset", required = false) Integer offset,
                                                 @RequestParam(value = "filter", required = false) String filter,
                                                 @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListCitiesShoplogistic(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }

    /**  Получение списка городов Spsr */
    @GetMapping("/lists/providerCities/spsr")
    public ResponseEntity getListCitiesSpsr(@RequestParam(value = "limit", required = false) Integer limit,
                                                    @RequestParam(value = "offset", required = false) Integer offset,
                                                    @RequestParam(value = "filter", required = false) String filter,
                                                    @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListCitiesSpsr(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }

    /**  Получение списка городов Cdek */
    @GetMapping("/lists/providerCities/cdek")
    public ResponseEntity getListCitiesCdek(@RequestParam(value = "limit", required = false) Integer limit,
                                            @RequestParam(value = "offset", required = false) Integer offset,
                                            @RequestParam(value = "filter", required = false) String filter,
                                            @RequestParam(value = "fields", required = false) String fields)
    {
        Mono<ListCitiesDto> answer = listsService.getListCitiesCdek(limit, offset, filter, fields);

        return ResponseEntity.ok(answer);
    }

    /**  Получение всех параметров подключения к службам доставки */
    @GetMapping("/lists/providers/connections")
    public ResponseEntity getListConnections(@RequestParam(value = "limit", required = false) Integer limit,
                                            @RequestParam(value = "offset", required = false) Integer offset,
                                            @RequestParam(value = "with", required = false) String with)
    {
        Mono<ListCitiesDto> answer = listsService.getListConnections(limit, offset, with);

        return ResponseEntity.ok(answer);
    }



    /**  Получение параметров подключения по ID */
    @GetMapping("/lists/providers/connections/{id}")
    public ResponseEntity getConnectionById(@PathVariable(value = "id") Integer id,
                                             @RequestParam(value = "with", required = false) String with)
    {
        Mono<String> answer = listsService.getConnectionById(id, with);

        return ResponseEntity.ok(answer);
    }
}
