package project.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import project.dto.calculateDto.CalculateDto;
import project.dto.calculateDto.DeliveryTypeDto;
import project.service.CalculateService;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
public class ApiCalculateController {

    @Autowired
    CalculateService calculateService;


    @PostMapping(value = "/calculator")
    public ResponseEntity<Mono<DeliveryTypeDto>> postOrder(@RequestBody CalculateDto calculate) {

        Mono<DeliveryTypeDto> answer = calculateService.postCalculator(calculate);

        return ResponseEntity.ok(answer);
    }

}
