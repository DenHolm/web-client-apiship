package project.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.ordersDto.*;
import project.service.LoginService;
import project.service.OrdersService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
public class ApiOrdersController {

    @Autowired
    OrdersService ordersService;

    @Autowired
    LoginService loginService;

    @PostMapping("/login")
    public ResponseEntity<TokenDto> login (@RequestBody LoginDto loginDto) throws ExecutionException, InterruptedException {
        return ResponseEntity.ok().body(loginService.login(loginDto));
    }

    @GetMapping(value = "/orders/{id}")
    public ResponseEntity getOrder(@PathVariable(value = "id") Integer id){

        Mono<PostOrderDto> answer = ordersService.getOrderById(id);

        return ResponseEntity.ok(answer);
    }

    @PostMapping(value = "/orders")
    public ResponseEntity postOrder(@RequestBody PostOrderDto postOrderDto) throws ExecutionException, InterruptedException {

        Mono<AccessOrderDto>answer = ordersService.postOrder(postOrderDto);

        return ResponseEntity.ok(answer);
    }

    @PostMapping(value = "/orders/validate")
    public ResponseEntity postValidateOrder(@RequestBody PostOrderDto postOrderDto){

        Mono<ValidateOrderDto>answer = ordersService.postValidateOrder(postOrderDto);

        return ResponseEntity.ok(answer);
    }

    @DeleteMapping(value = "/orders/{id}")
    public ResponseEntity deleteOrder(@PathVariable("id") Long id){

        Mono<DeletedOrderDto> answer = ordersService.deleteOrder(id);

        return ResponseEntity.ok(answer);
    }

    @PutMapping(value = "/orders/{id}")
    public ResponseEntity changeOrder(@PathVariable("id") Long id, @RequestBody PostOrderDto postOrderDto){

        Mono<AccessMessageDto> answer = ordersService.changeOrder(id, postOrderDto);

        return ResponseEntity.ok(answer);
    }

    @GetMapping(value = "/orders/{id}/cancel")
    public ResponseEntity getCancelOrder(@PathVariable(value = "id") Long id){

        Mono<CanceledOrderDto> answer = ordersService.cancelOrder(id);

        return ResponseEntity.ok(answer);
    }

    @GetMapping(value = "/orders/{id}/status")
    public ResponseEntity getStatusOrder(@PathVariable(value = "id") Long id){

        Mono<StatusOrderDto> answer = ordersService.getStatusOrder(id);

        return ResponseEntity.ok(answer);
    }

    @PostMapping(value = "/orders/{id}/items")
    public ResponseEntity postItemsOrder(@PathVariable(value = "id") Long id, @RequestBody ListItemsDto items){

        Mono<AccessMessageDto>answer = ordersService.changeItemsOrder(id, items);

        return ResponseEntity.ok(answer);
    }

    /**Получение статуса заказа по номеру заказа в системе клиента*/
    @GetMapping(value = "/orders/status")
    public ResponseEntity getStatusOrderByClientNumber(@RequestParam(value = "clientNumber") String number){

        Mono<StatusOrderDto> answer = ordersService.getStatusOrderByClientNumber(number);

        return ResponseEntity.ok(answer);
    }

    @PostMapping(value = "/orders/{id}/resend")
    public ResponseEntity postResendOrder(@PathVariable(value = "id") Long id){

        Mono<AccessOrderDto>answer = ordersService.postResendOrder(id);

        return ResponseEntity.ok(answer);
    }

    @GetMapping(value = "/orders/status/history")
    public ResponseEntity getStatusHistoryOrderByClientNumber(@RequestParam(value = "clientNumber") String number){

        Mono<ListStatusOrderDto> answer = ordersService.getStatusHistoryOrderByClientNumber(number);

        return ResponseEntity.ok(answer);
    }

    @PostMapping(value = "/orders/statuses")
    public ResponseEntity postStatusesOrder(@RequestBody ListOrderIdsDto orderIds){
        Mono<ListSucceedOrdersDto>answer = ordersService.postStatusesOrder(orderIds);
        return ResponseEntity.ok(answer);
    }


   /* @GetMapping(value = "/lists/services")
    public ResponseEntity<Mono<String>> getListStatuses(@RequestParam("key") String key){
        return ResponseEntity.ok(ordersService.listStatuses(key));
    }*/

    @GetMapping(value = "/orders/statuses/date/{date}")
    public ResponseEntity getOrdersStatusesByDate(@PathVariable("date") String date){
        Flux<StatusOrderDto> answer = ordersService.getOrderStatusesByDate(date);
        return ResponseEntity.ok(answer);
    }

    @GetMapping(value = "/orders/statuses/history/date/{date}", params = {"offset", "limit"})
    public ResponseEntity getOrderHistoryStatusesByDate(@PathVariable("date") String date,
                                                        @RequestParam("offset") Integer offset,
                                                        @RequestParam("limit") Integer limit){

        Mono<ListRowsHistoryStatusesDto> answer = ordersService.getOrderHistoryStatusesByDate(date, offset, limit);

        return ResponseEntity.ok(answer);
    }

    @GetMapping(value = "/orders/{orderId}/statusHistory", params = {"offset", "limit"})
    public ResponseEntity getHistoryStatusOfOrders(@PathVariable("orderId") Long orderId,
                                                   @RequestParam("offset") Integer offset,
                                                   @RequestParam("limit") Integer limit){

        Mono<OrderHistoryStatusesDto> answer = ordersService.getHistoryStatusOfOrders(orderId, offset, limit);

        return ResponseEntity.ok(answer);
    }

    //@PostMapping(value = "orders/upload")

    @GetMapping(value = "/orders/statuses/interval")
    public ResponseEntity getIntervalStatusesOfOrders(@RequestParam(value = "from") String from,
                                                   @RequestParam(value = "to") String to,
                                                   @RequestParam(value = "filter", required = false) String filter,
                                                   @RequestParam("offset") Integer offset,
                                                   @RequestParam("limit") Integer limit){

        Mono<ListRowsHistoryStatusesDto> answer = ordersService.getIntervalStatusesOfOrders(from, to, filter, offset, limit);

        return ResponseEntity.ok(answer);
    }

    @PostMapping(value = "/orders/labels")
    public ResponseEntity postLabels(@RequestBody ListOrderIdsAndFormatDto list){
        Mono<GetLabelDto>answer = ordersService.postLabels(list);
        return ResponseEntity.ok(answer);
    }

    @PostMapping(value = "/orders/waybills")
    public ResponseEntity postWaybillItems(@RequestBody ListOrderIdsDto orderIds){
        Mono<WaybillItemsDto>answer = ordersService.postWaybillItems(orderIds);
        return ResponseEntity.ok(answer);
    }


}
