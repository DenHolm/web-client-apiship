package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import project.dto.errorDto.ErrorBodyDto;
import project.dto.listsDto.ConnectionDto;
import project.dto.listsDto.ListCitiesDto;
import project.dto.listsDto.TypesDto;
import project.dto.listsDto.ServicesDto;
import project.dto.ordersDto.TokenDto;
import project.exceptions.MyException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ListsService {

    private final TokenDto tokenDto;

    private final WebClient client;

    @Autowired
    public ListsService(LoginService loginService, WebClient client) {
        this.client = client;
        tokenDto = loginService.getTokenDto();
    }

    /** Получение истории изменения всех статусов по заданному интервалу
     * Ошибка на сайте apiship поле fields бросает ошибку 500 */
    public Mono<ListCitiesDto> getListStatuses(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/lists/statuses")
                                .queryParams(params)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(ListCitiesDto.class);
    }

    /** Получение списка поставщиков услуг
    * Ошибка на сайте apiship поле fields бросает ошибку 500 */
    public Mono<ListCitiesDto> getListProviders(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/lists/providers")
                                .queryParams(params)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(ListCitiesDto.class);
    }

    /** Получение списка дополнительных услуг
     * документация apiship не совпадает с типом response*/
    public Flux<ServicesDto> getListServices(String providerKey){

        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/lists/services")
                                .queryParam("providerKey",providerKey)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToFlux(ServicesDto.class);
    }

    /** Получение возможных параметров для подключения к службе доставки
     * Параметры подключения для каждого параметра разные поэтому использую String.class*/
    public Mono<String> getListProvidersParams(String providerKey){

        return client.get()
                .uri("/lists/providers/{providerKey}/params", providerKey)
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(String.class);
    }

    /** Получение списка пунктов приема/выдачи */
    public Mono<ListCitiesDto> getListPoints(Integer limit, Integer offset, String filter,
                                             String fields, Boolean stateCheckOff){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        if(stateCheckOff != null){
            params.add("stateCheckOff", stateCheckOff.toString());
        }

        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/lists/points")
                                .queryParams(params)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(ListCitiesDto.class);
    }

    /** Получение списка актуальных тарифов */
    public Mono<ListCitiesDto> getListTariffs(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/lists/tariffs")
                                .queryParams(params)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(ListCitiesDto.class);
    }

    /**  Получение списка типов приема */
    public Flux<TypesDto> getPickupTypes(){

        return client.get()
                .uri("/lists/pickupTypes")
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToFlux(TypesDto.class);
    }

    /**  Получение списка типов доставки */
    public Flux<TypesDto> getDeliveryTypes(){

        return client.get()
                .uri("/lists/deliveryTypes")
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToFlux(TypesDto.class);
    }

    /**  Получение списка способов оплаты */
    public Flux<TypesDto> getPaymentMethods(){

        return client.get()
                .uri("/lists/paymentMethods")
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToFlux(TypesDto.class);
    }

    /** Получение списка типов операций для точек приема/выдачи товаров */
    public Flux<TypesDto> getOperationTypes(){

        return client.get()
                .uri("/lists/operationTypes")
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToFlux(TypesDto.class);
    }

    /** Получение списка типов операций для точек приема/выдачи товаров */
    public Flux<TypesDto> getPointTypes(){

        return client.get()
                .uri("/lists/pointTypes")
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToFlux(TypesDto.class);
    }

    /** Получение списка городов b2cpl */
    public Mono<ListCitiesDto> getListCitiesB2cpl(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return getListCities("b2cpl", params);
    }

    /** Получение списка городов boxberry */
    public Mono<ListCitiesDto> getListCitiesBoxberry(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return getListCities("boxberry", params);
    }

    /**  Получение списка городов dpd */
    public Mono<ListCitiesDto> getListCitiesDpd(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return getListCities("dpd", params);
    }

    /**  Получение списка городов iml */
    public Mono<ListCitiesDto> getListCitiesIml(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return getListCities("iml", params);
    }

    /**  Получение списка городов Maxi */
    public Mono<ListCitiesDto> getListCitiesMaxi(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return getListCities("maxi", params);
    }

    /**  Получение списка городов Pickpoint */
    public Mono<ListCitiesDto> getListCitiesPickpoint(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return getListCities("pickpoint", params);
    }

    /**  Получение списка городов Shoplogistic */
    public Mono<ListCitiesDto> getListCitiesShoplogistic(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return getListCities("shoplogistic", params);
    }

    /**  Получение списка городов Spsr */
    public Mono<ListCitiesDto> getListCitiesSpsr(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return getListCities("spsr", params);
    }

    /**  Получение списка городов Cdek */
    public Mono<ListCitiesDto> getListCitiesCdek(Integer limit, Integer offset, String filter, String fields){

        MultiValueMap<String, String> params = createParams(limit,offset,filter, fields);

        return getListCities("cdek", params);
    }

    /**  Получение списка городов Connections */
    public Mono<ListCitiesDto> getListConnections(Integer limit, Integer offset, String with){

        MultiValueMap<String, String> params = createParams(limit,offset);
        params.add("with", with);
        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/lists/providers/connections")
                                .queryParams(params)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(ListCitiesDto.class);
    }

    /**  Получение списка городов Connections по id */
    public Mono<String> getConnectionById(Integer id, String with){


        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/lists/providers/connections/" + id)
                                .queryParam("with", with)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(String.class);
    }

    /**  Получение списка городов Connections */
    private Mono<ListCitiesDto> getListCities(String name, MultiValueMap params){
        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/lists/providerCities/" + name)
                                .queryParams(params)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(ListCitiesDto.class);
    }

    private MultiValueMap<String, String> createParams(Integer limit, Integer offset, String filter, String fields) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(4);

        if (limit != null) {
            params.add("limit", limit.toString());
        }
        if (offset != null) {
            params.add("offset", offset.toString());
        }
        if (filter != null) {
            params.add("filter", filter);
        }
        if (fields != null) {
            params.add("fields", fields);
        }
        return params;
    }

    private MultiValueMap<String, String> createParams(Integer limit, Integer offset) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(4);

        if (limit != null) {
            params.add("limit", limit.toString());
        }
        if (offset != null) {
            params.add("offset", offset.toString());
        }
        return params;
    }
}
