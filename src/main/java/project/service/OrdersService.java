package project.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import project.dto.errorDto.ErrorBodyDto;
import project.dto.ordersDto.*;
import project.exceptions.MyException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class OrdersService
{
    private final TokenDto tokenDto;

    private final WebClient client;

    @Autowired
    public OrdersService(LoginService loginService, WebClient client) {
        this.client = client;
        tokenDto = loginService.getTokenDto();
    }


    /** Создание заказа */
    public Mono<AccessOrderDto> postOrder(PostOrderDto order) {
        return  client.post()
                .uri("/orders/")
                .header("Authorization", tokenDto.getAccessToken())
                .body(Mono.just(order), PostOrderDto.class)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body)))
                .bodyToMono(AccessOrderDto.class);
    }

    /** Валидация заказа */
    public Mono<ValidateOrderDto> postValidateOrder(PostOrderDto order){
        return client.post()
                .uri("/orders/validate")
                .header("Authorization", tokenDto.getAccessToken())
                .body(Mono.just(order), PostOrderDto.class)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body-> new MyException(body)))
                .bodyToMono(ValidateOrderDto.class);
    }

    /** Получение информации по заказу */
    public Mono<PostOrderDto> getOrderById(Integer id){
        return client.get()
                .uri("/orders/{id}", id)
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(PostOrderDto.class);
    }

    /** Удаление заказа */
    public Mono<DeletedOrderDto> deleteOrder(Long id){
        return client.delete()
                .uri("/orders/{id}", id)
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body-> new MyException(body)))
                .bodyToMono(DeletedOrderDto.class);
    }


    /** Изменение заказа */
    public Mono<AccessMessageDto> changeOrder(Long id, PostOrderDto order){
        return client.put()
                .uri("/orders/{id}", id)
                .header("Authorization", tokenDto.getAccessToken())
                .body(Mono.just(order), PostOrderDto.class)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body-> new MyException(body)))
                .bodyToMono(AccessMessageDto.class);
    }

    /** Повторная отправка заказа в СД */
    public Mono<AccessOrderDto> postResendOrder(Long id){
        return client.post()
                .uri("/orders/{id}/resend", id)
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body-> new MyException(body)))
                .bodyToMono(AccessOrderDto.class);
    }

    /** Отмена заказа */
    public Mono<CanceledOrderDto> cancelOrder(Long id){
        return client.get()
                .uri("/orders/{id}/cancel",id)
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(CanceledOrderDto.class);
    }

    /** Получение статуса заказа */
    public Mono<StatusOrderDto> getStatusOrder(Long id){
        return client.get()
                .uri("/orders/{id}/status",id)
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(StatusOrderDto.class);
    }

    /** Обновление товаров заказа */
    public Mono<AccessMessageDto> changeItemsOrder(Long id, ListItemsDto items){
        return client.post()
                .uri("/orders/{id}/items", id)
                .header("Authorization", tokenDto.getAccessToken())
                .body(Mono.just(items), ListItemsDto.class)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body-> new MyException(body)))
                .bodyToMono(AccessMessageDto.class);
    }

    /** Получение статуса заказа по номеру заказа в системе клиента */
    public Mono<StatusOrderDto> getStatusOrderByClientNumber(String clientNumber){
        return client.get()
                .uri(uriBuilder ->
                    uriBuilder.path("/orders/status")
                    .queryParam("clientNumber", clientNumber)
                    .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(StatusOrderDto.class);
    }


    /**Получение истории статуса заказа по номеру заказа в системе клиента*/
    public Mono<ListStatusOrderDto> getStatusHistoryOrderByClientNumber(String clientNumber){
        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/orders/status/history")
                                .queryParam("clientNumber", clientNumber)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(ListStatusOrderDto.class);
    }

    /** Получение статусов по нескольким заказам */
    public Mono<ListSucceedOrdersDto> postStatusesOrder(ListOrderIdsDto orderIds){
        return client.post()
                .uri("/orders/statuses")
                .header("Authorization", tokenDto.getAccessToken())
                .body(Mono.just(orderIds), ListOrderIdsDto.class)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body-> new MyException(body)))
                .bodyToMono(ListSucceedOrdersDto.class);
    }


    /** Получение измененных статусов по всем заказам клиента (company) после указанной в методе даты */
    public Flux<StatusOrderDto> getOrderStatusesByDate(String date){
        return client.get()
                .uri("/orders/statuses/date/{date}", date)
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToFlux(StatusOrderDto.class);
    }

    /** Получение истории изменения всех статусов с определенной даты */
    public Mono<ListRowsHistoryStatusesDto> getOrderHistoryStatusesByDate(String date, Integer offset, Integer limit){
        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/orders/statuses/history/date/" + date)
                                .queryParam("offset", offset)
                                .queryParam("limit", limit)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(ListRowsHistoryStatusesDto.class);
    }

    /** Получение истории статусов заказа */
    public Mono<OrderHistoryStatusesDto> getHistoryStatusOfOrders(Long orderId, Integer offset, Integer limit){
        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/orders/" + orderId + "/statusHistory")
                                .queryParam("offset", offset)
                                .queryParam("limit", limit)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(OrderHistoryStatusesDto.class);
    }

    /** Получение истории изменения всех статусов по заданному интервалу (Доработка) */
    public Mono<ListRowsHistoryStatusesDto> getIntervalStatusesOfOrders(String from, String to,
                                                                        String filter, Integer offset,
                                                                        Integer limit){
        return client.get()
                .uri(uriBuilder ->
                        uriBuilder.path("/orders/statuses/interval")
                                .queryParam("from", from)
                                .queryParam("to",to)
                                .queryParam("filter", filter)
                                .queryParam("offset", offset)
                                .queryParam("limit", limit)
                                .build())
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body))
                )
                .bodyToMono(ListRowsHistoryStatusesDto.class);
    }

    /** Получение ярлыков для заказов */
    public Mono<GetLabelDto> postLabels(ListOrderIdsAndFormatDto orderIds){
        return client.post()
                .uri("/orders/labels")
                .header("Authorization", tokenDto.getAccessToken())
                .body(Mono.just(orderIds), ListOrderIdsAndFormatDto.class)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body-> new MyException(body)))
                .bodyToMono(GetLabelDto.class);
    }

    /** Получение актов приема-передачи заказов */
    public Mono<WaybillItemsDto> postWaybillItems(ListOrderIdsDto orderIds){
        return client.post()
                .uri("/orders/waybills")
                .header("Authorization", tokenDto.getAccessToken())
                .body(Mono.just(orderIds), ListOrderIdsDto.class)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body-> new MyException(body)))
                .bodyToMono(WaybillItemsDto.class);
    }

}
