package project.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import project.dto.errorDto.ErrorBodyDto;
import project.dto.ordersDto.LoginDto;
import project.dto.ordersDto.TokenDto;
import project.exceptions.MyException;
import reactor.core.publisher.Mono;

import java.util.concurrent.ExecutionException;

@Slf4j
@Service
public class LoginService {

    @Autowired
    WebClient client;

    private TokenDto tokenDto = new TokenDto("1eb0a2ecc9b6a436cd3cab18eccd02e8", "2020-05-08T23:38:27+03:00");

    public TokenDto getTokenDto() {
        return tokenDto;
    }


    /** Получение токена */
    public TokenDto login(LoginDto dto) throws ExecutionException, InterruptedException {
        return tokenDto = client.post()
                .uri("login")
                .body(Mono.just(dto), LoginDto.class)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body)))
                .bodyToMono(TokenDto.class)
                .toFuture().get();

    }
}
