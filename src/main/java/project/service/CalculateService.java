package project.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import project.dto.calculateDto.CalculateDto;
import project.dto.calculateDto.DeliveryTypeDto;
import project.dto.errorDto.ErrorBodyDto;
import project.dto.ordersDto.TokenDto;
import project.exceptions.MyException;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class CalculateService {

    private final TokenDto tokenDto;

    private final WebClient client;

    @Autowired
    public CalculateService(LoginService loginService, WebClient client) {
        this.client = client;
        tokenDto = loginService.getTokenDto();
    }

    /** Создание заказа */
    public Mono<DeliveryTypeDto> postCalculator(CalculateDto calculate) {
        return  client.post()
                .uri("/calculator")
                .header("Authorization", tokenDto.getAccessToken())
                .body(Mono.just(calculate), CalculateDto.class)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body)))
                .bodyToMono(DeliveryTypeDto.class);
    }
}
