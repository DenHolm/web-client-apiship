package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import project.dto.courierCallDto.CourierCallCanceled;
import project.dto.courierCallDto.CourierCallCreated;
import project.dto.courierCallDto.CourierCallDto;
import project.dto.errorDto.ErrorBodyDto;
import project.dto.ordersDto.TokenDto;
import project.exceptions.MyException;
import reactor.core.publisher.Mono;

@Service
public class CourierCallService {

    private final TokenDto tokenDto;

    private final WebClient client;

    @Autowired
    public CourierCallService(LoginService loginService, WebClient client) {
        this.client = client;
        tokenDto = loginService.getTokenDto();
    }

    /** Создание заявки на вызов курьера */
    public Mono<CourierCallCreated> postCourierCall(CourierCallDto courierDto) {
        return  client.post()
                .uri("/courierCall")
                .header("Authorization", tokenDto.getAccessToken())
                .body(Mono.just(courierDto), CourierCallDto.class)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body)))
                .bodyToMono(CourierCallCreated.class);
    }

    /** Отмена заявки на вызов курьера */
    public Mono<CourierCallCanceled> postCancelCourier(Long courierCallId) {
        return  client.post()
                .uri("/courierCall/{courierCallId}/cancel", courierCallId)
                .header("Authorization", tokenDto.getAccessToken())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        response -> response
                                .bodyToMono(ErrorBodyDto.class)
                                .map(body -> new MyException(body)))
                .bodyToMono(CourierCallCanceled.class);
    }
}
