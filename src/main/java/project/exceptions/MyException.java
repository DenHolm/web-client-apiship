package project.exceptions;

import lombok.Data;
import project.dto.errorDto.ErrorBodyDto;

@Data
public class MyException extends RuntimeException
{
    private ErrorBodyDto error;

    public MyException(String message) {
        super(message);
    }

    public MyException(ErrorBodyDto error) {
        this.error = error;

    }
}
